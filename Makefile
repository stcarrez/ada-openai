ALR?=alr --non-interactive
OPENAPI=$(ALR) exec -- openapi-generator
OPENAPI_OPTIONS=--additional-properties projectName=openai \
                --additional-properties openApiName=OpenAPI \
                --additional-properties httpSupport=Curl \
                --model-package OpenAI -o .

build:
	$(ALR) build

generate:
	$(OPENAPI) generate --generator-name ada -i openai.yaml $(OPENAPI_OPTIONS)

clean:
	rm -rf bin obj
